
import sqlite3

# Create one more Table called Departments with two columns Department_id and Department_name
conn= sqlite3.connect('Employee And Departments.db')
c=conn.cursor()

c.execute("""
        CREATE TABLE Departments (
        DEPT_NAME text NOT NULL,
        DEPT_ID integer NOT NULL,
        FOREIGN KEY (DEPT_ID) REFERENCES Employee (DEPT_ID))
        """)

# Insert 5 records into this table
insert_dept = [
    ('PRODUCTION',1),
    ('RESEARCH',2),
    ('ACCOUNTING',3),
    ('HR',4),
    ('PURCHASING',5)
        ]
c.executemany("INSERT INTO Departments (DEPT_NAME, DEPT_ID) VALUES (?,?)",insert_dept)

# Print the details of all the employees in a particular department (Department_id is input by the user)
give_id=int(input('Enter the id: '))
c.execute("SELECT * FROM Employee INNER JOIN Departments ON Employee.DEPT_ID = Departments.DEPT_ID WHERE Departments.DEPT_ID=(?)",(give_id,))
print(c.fetchall())

conn.commit()
conn.close()
