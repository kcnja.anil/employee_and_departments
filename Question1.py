import sqlite3

conn= sqlite3.connect('Employee And Departments.db')
c=conn.cursor()

# Create a Table Employee which will have the four columns, i.e., Name, ID, salary, and Department_id
c.execute("""
        CREATE TABLE Employee (
        NAME text NOT NULL,
        ID integer NOT NULL,
        SALARY integer NOT NULL,
        DEPT_ID integer NOT NULL)
        """)
# Add a new column ‘City’ to the Table Employee
c.execute(" ALTER TABLE Employee ADD COLUMN CITY text")
# Insert 5 records into this table
insert = [
    ('Gary',101,25000,1,'Ottawa'),
    ('Martha',102,30000,3,'Hamilton'),
    ('Louis',105,15000,5,'Ottawa'),
    ('Amy',112,20000,4,'Hamilton'),
    ('Jenna',104,15000,2,'Hamilton')
        ]
c.executemany("INSERT INTO Employee (NAME, ID, SALARY, DEPT_ID, CITY) VALUES (?,?,?,?,?)",insert)

# Read  the Name, ID, and Salary from the Employee table and print it
c.execute("SELECT NAME, ID, SALARY FROM Employee")
print(c.fetchall())

# Print the details of employees whose names start with ‘j’ (or any letter input by the user)
start_with = input('Enter first letter of employee name')
start_with= start_with+'%'
c.execute("SELECT * FROM Employee WHERE NAME LIKE (?)",(start_with,))
print(c.fetchall())

# Print the details of employees with ID’s inputted by the user
emp_id = int(input('Enter Employee ID'))
c.execute("SELECT * FROM Employee WHERE ID = (?)",(emp_id,))
print(c.fetchall())

# Change the name of the employee whose ID is input by the user
change_id = int(input('Enter Employee ID to change Name'))
name = input('What is the name?')
c.execute("UPDATE Employee SET NAME = (?) WHERE ID = (?)",(name, change_id))
c.execute("SELECT * FROM Employee WHERE ID = (?)",(change_id,))
print(c.fetchall())


conn.commit()
conn.close()
